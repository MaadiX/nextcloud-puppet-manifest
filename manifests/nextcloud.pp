class helpers::nextcloud (
  $enabled           = str2bool($::nextcloud_enabled),
  $trusted_domains   = '',
  $adminuser         = undef,
  $version           = '',
  $version_installed = undef,
  $datadir           = '/var/www/nextcloud/data',
) {


  validate_bool($enabled)

  if $enabled {

    #nextcloud dependencies
    if ! defined(Package['php5-gd']) {
      ensure_resource('package','php5-gd', {
      ensure  => present,
      })
    }
    if ! defined(Package['php5-intl']) {
      ensure_resource('package','php5-intl', {
      ensure  => present,
      })
    }
    if ! defined(Package['php5-mcrypt']) {
      ensure_resource('package','php5-mcrypt', {
      ensure  => present,
      })
    }
    if ! defined(Package['php5-imagick']) {
      ensure_resource('package','php5-imagick', {
      ensure  => present,
      })
    }
    if ! defined(Package['php5-apcu']) {
      ensure_resource('package','php5-apcu', {
      ensure  => present,
      })
    }

    ##nextclouddb user passw file
    exec { 'nextclouddb passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/nextclouddb'",
      creates => '/etc/maadix/nextclouddb',
      require => Package['pwgen'],
    }

    ##nextcloud user passw file
    exec { 'nextcloud passw':
      command => "/bin/bash -c 'umask 077 && pwgen 10 -1 | tr -d \"\n\" > /etc/maadix/nextcloud'",
      creates => '/etc/maadix/nextcloud',
      require => Package['pwgen'],
    }

    if $adminuser != undef {
      ##nextcloud script
      file { 'nextcloud password':
        path    => '/etc/maadix/scripts/nextcloudpass.sh',
        owner   => 'root',
        group   => 'root',
        mode    => '0700',
        content => template('helpers/nextcloudpass.sh.erb'),
      }
    }

    ##nextcloud pass mail script
    file { 'nextcloud mail password':
      path    => '/etc/maadix/mail/nextcloud.sh',
      owner   => 'root',
      group   => 'root',
      mode    => '0700',
      content => template('helpers/nextcloud_mail_password.sh.erb'),
    }

    #nextcloud directory
    file { '/var/www/nextcloud':
      ensure => directory,
      mode   => '0755',
      group   => 'www-data',
      owner   => 'www-data',
    }

    #nextcloud data directory
    file { "$datadir":
      ensure => directory,
      mode   => '0770',
      group   => 'www-data',
      owner   => 'www-data',
      require => File['/var/www/nextcloud'],
    }

    #nextcloud source
    archive { "/var/www/nextcloud/nextcloud-$version.tar.bz2":
      ensure        => present,
      extract       => true,
      extract_path  => '/var/www/nextcloud',
      source        => "https://download.nextcloud.com/server/releases/nextcloud-$version.tar.bz2",
      creates       => '/var/www/nextcloud/nextcloud',
      cleanup       => false,
      user          => 'www-data',
      require       => File["$datadir"],
      before        => Exec['nextcloud db and occ installation'],
    }->
    #disable web updater by removing updater scripts
    file { "/var/www/nextcloud/nextcloud/updater":
      ensure  => absent,
      force   => true,
      recurse => true,
      purge   => true,
    }

    #nextcloud db and installation
    exec { 'nextcloud db and occ installation':
      command => "/bin/bash /etc/maadix/scripts/nextcloudpass.sh",
      creates => '/etc/maadix/status/nextcloud',
      require => File['nextcloud password'],
      notify  => Exec['set nextcloud installed version'],
    }


    #upgrade: prepare to update if version changes / stop apache and backup folder
    #upgrade: copy current config file and 3rd party apps to new installation and launch occ upgrade
    if($version_installed != $version) and ($version_installed != ''){
      exec { 'updating nextcloud: stop apache and backup directories':
        command    => "service apache2 stop && mv /var/www/nextcloud/nextcloud /var/www/nextcloud/nextcloud-$version_installed",
        logoutput  => true,
        onlyif     => "test -d /var/www/nextcloud/nextcloud",
        before     => Archive["/var/www/nextcloud/nextcloud-$version.tar.bz2"],
      } ->
      exec { 'updating nextcloud: ddbb backup':
        command    => "mysqldump --defaults-extra-file=/root/.my.cnf nextcloud > /var/www/nextcloud/nextcloud-$version_installed.sql && chmod 700 /var/www/nextcloud/nextcloud-$version_installed.sql",
        logoutput  => true,
        before     => Archive["/var/www/nextcloud/nextcloud-$version.tar.bz2"],
      }
      exec { 'updating nextcloud: copying 3rd party apps':
        command    => "find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c \"if [ ! -d /var/www/nextcloud/nextcloud/apps/% ]; then echo % && cp -Rp % /var/www/nextcloud/nextcloud/apps/; fi\" ",
        cwd        => "/var/www/nextcloud/nextcloud-$version_installed/apps",
        logoutput  => true,
        creates    => "/var/www/nextcloud/nextcloud/config/config.php",
        require    => Archive["/var/www/nextcloud/nextcloud-$version.tar.bz2"],
      } ->
      exec { 'updating nextcloud: copying config file':
        command    => "sudo -u www-data cp /var/www/nextcloud/nextcloud-$version_installed/config/config.php /var/www/nextcloud/nextcloud/config/config.php",
        logoutput  => true,
      } ->
      exec { 'updating nextcloud: occ upgrade':
        command    => "sudo -u www-data php occ upgrade",
        cwd        => "/var/www/nextcloud/nextcloud",
        logoutput  => true,
        timeout    => 28800,
        notify     => [
                      Service['httpd'],
                      Exec['set nextcloud installed version'],
                      ],
      } ->
      exec { 'updating nextcloud: restart apache':
        command    => "service apache2 restart",
        before     => [
                      Exec['nextcloud trusted_domains'],
                      Exec['nextcloud opcache'],
                      Exec['nextcloud disable web upgrade'],
                      Exec['nextcloud disable update checker'],
                      ],
      }      
    }

    #set NC options on first install or on version update
    if($version_installed != $version) {
      #nextcloud options
      exec { 'nextcloud trusted_domains':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
      }
      exec { 'nextcloud opcache':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set memcache.local --value='\OC\Memcache\APCu'",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud disable web upgrade':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set upgrade.disable-web --value=true --type=boolean",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud disable update checker':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set updatechecker --value=false --type=boolean",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud mail domain':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set mail_domain --value='$::fqdn'",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud mail from':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set mail_from_address --value='nextcloud'",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud mail mail_smtpmode':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpmode --value='php'",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
      exec { 'nextcloud mail mail_smtpauthtype':
        command => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set mail_smtpauthtype --value='LOGIN'",
        require => [
                   Exec['nextcloud db and occ installation'],
                   ],
        notify  => Service['httpd'],
      }
    }

    #set installed version only run when first installation or upgrade are ok
    exec { 'set nextcloud installed version':
      command     => "/etc/maadix/scripts/setldapgroupfield.sh nextcloud version $version",
      logoutput   => true,
      refreshonly => true,
    }

    ## fqdn change scripts
    exec { 'nextcloud fqdn change trusted_domains':
      command     => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set trusted_domains 0 --value=$trusted_domains",
      require     => [
                     Exec['nextcloud db and occ installation'],
                     ],
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      notify      => [
                     Service['httpd'],
                     ]
    }
    exec { 'nextcloud fqdn change mail domain':
      command     => "sudo -u www-data php /var/www/nextcloud/nextcloud/occ config:system:set mail_domain --value='$::fqdn'",
      logoutput   => true,
      subscribe   => [
                     File['conf fqdn'],
                     ],
      refreshonly => true,
      require     => [
                     Exec['nextcloud db and occ installation'],
                     ],
      notify      => Service['httpd'],
    }


    #php opcache conf
    file { 'php5 opcache conf':
      path    => '/etc/php5/apache2/conf.d/90-opcache.ini',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/90-opcache.ini.erb'),
      notify  => Service['httpd'],
      require => Package['php5-apcu'],
    }

    #apache conf
    file { 'apache nextcloud.conf':
      path    => '/etc/apache2/conf-available/nextcloud.conf',
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      content => template('helpers/nextcloud.conf.erb'),
      notify => Service['httpd'],
    } ->
    file { '/etc/apache2/conf.d/nextcloud.conf':
      ensure => 'link',
      target => '/etc/apache2/conf-available/nextcloud.conf',
      notify => Service['httpd'],
    }



    ########### upgrade ########################################
    #backup mysql / todo
    #disable 3rd party apps / todo
    #stop apache / done
    #rename current nextcloud to nexcloud-$version_installed / done
    #download and extract new version to nextcloud / done
    #copy config.php from current version to new installation / done
    #copy non-existent 3rd party apps from current version to new installation /done
    ##find * -type d -prune -print0 | xargs -0 -n1 -I % sh -c 'echo %; mkdir /var/www/nextcloud/nextcloud/apps/% && echo % && cp -Rp % /var/www/nextcloud/nextcloud/apps/'
    #start apache / done
    #launch occ upgrade with HIGH TIMEOUT / done
    ##sudo -u www-data php occ upgrade
    #enable 3rd party apps / todo

  }

}

