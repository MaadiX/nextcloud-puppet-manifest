# Nextcloud Puppet manifest

Custom Puppet manifest to install Nextcloud with MaadiX  


This Puppet manifest installs Nextcloud without the need to know Nextcloud database and user password, which are generated locally on the target server.
It must be used on a server that already has Apache , Mysql and php installed, as it doesn't check for these dependencies.  

It includes:  

 * A template to create an Apache vhost, to run as an alias  
 * A template to create the Mysql database and set the admin user password  
 * A template to send an email to the user with the data to access the Nextcloud installation    
